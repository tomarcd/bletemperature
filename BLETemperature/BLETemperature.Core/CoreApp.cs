﻿using System.Threading;
using Acr.UserDialogs;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using Plugin.BluetoothLE;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace BLETemperature.Core
{
    public class CoreApp : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();


            Mvx.RegisterSingleton<IUserDialogs>(() => UserDialogs.Instance);
            Mvx.RegisterSingleton<IAdapter>(() => CrossBleAdapter.Current);
            Mvx.RegisterSingleton<IPermissions>(() =>CrossPermissions.Current);
             
            RegisterNavigationServiceAppStart<ViewModels.ScanForDevicesViewModel>();
        }
    }
}
