﻿using System;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;

namespace BLETemperature.Core
{
    public static class MvxObservableCollectionExtension
    {
        public static void Sort<T>(this MvxObservableCollection<T> collection, Comparison<T> comparison)
        {
            var sortableList = new List<T>(collection);
            sortableList.Sort(comparison);

            for (int i = 0; i < sortableList.Count; i++)
            {
                collection.Move(collection.IndexOf(sortableList[i]), i);
            }
        }
    }
}
