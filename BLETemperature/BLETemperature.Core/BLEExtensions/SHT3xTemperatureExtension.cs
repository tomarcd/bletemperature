﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BluetoothLE;

namespace BLETemperature.Core.BLEExtensions
{

    public static class SHT3xTemperatureExtension
    {
        public static Guid SHT3xTemperatureServiceUuid = new Guid("00002234-b38d-4985-720e-0f993a68ee41");

        private static String SHT3XTEMPERATURE_LEVEL_CHARACTERISTIC_UUID = "00002235-b38d-4985-720e-0f993a68ee41";

        public static String SHT3XTEMPERATURE_LEVEL_UNIT = "°C";


        public static IObservable<IScanResult> ScanForSHT3xTemperatureService(this IAdapter adapter)
        {
            return adapter
                .Scan()
                .Where(x => x.AdvertisementData.ServiceUuids?.Contains(SHT3xTemperatureServiceUuid) ?? false);
        }


        public static async Task<bool> HasSHT3xTemperatureService(this IDevice device)
        {
            AssertTemperatureServiceConnected(device);
            var character = await FindTemperatureServiceCharacteristic(device);
            return character != null;
        }


        public static IObservable<float> WhenSHT3xTemperatureLevelChange(this IDevice device)
        {
            AssertTemperatureServiceConnected(device);

            return Observable.Create<float>(async ob =>
            {
                IDisposable token = null;
                var ch = await FindTemperatureServiceCharacteristic(device);

                if (ch == null)
                {
                    ob.OnError(new ArgumentException("Device does not appear to be a SHT3xHumidity"));
                }
                else
                {

                    token = ch
                        //.ReadInterval(TimeSpan.FromSeconds(3))
                        .WhenReadOrNotify(TimeSpan.FromSeconds(3))
                        .Subscribe(result =>
                        {
                            if (result.Data.Length < 4 * 2 || result.Data.Length % 4 > 0)
                            {
                                var temperatureLevel = result.Data.ExtractFloat(0);
                                ob.OnNext(temperatureLevel);
                            }
                        });
                }
                return () => token?.Dispose();
            });
        }


        static void AssertTemperatureServiceConnected(IDevice device)
        {
            if (device.Status != ConnectionStatus.Connected)
                throw new ArgumentException("Device must be connected");
        }

        public static async Task<IGattCharacteristic> FindTemperatureServiceCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(SHT3xTemperatureServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                //.Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(SHT3XTEMPERATURE_LEVEL_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }
    }
}
