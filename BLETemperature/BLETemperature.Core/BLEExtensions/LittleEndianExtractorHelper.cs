﻿using System;
namespace BLETemperature.Core.BLEExtensions
{
    public static class LittleEndianExtractorHelper
    {
        public static float ExtractFloat(this byte[] byteValue,  int offset)
        {
            byte[] target = new byte[4];
            Array.Copy(byteValue, offset, target, 0, target.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(target);
            }
            return BitConverter.ToSingle(target, 0);
        }

        public static int ExtractInt(this byte[] byteValue, int offset)
        {
            byte[] target = new byte[4];
            Array.Copy(byteValue, offset, target, 0, target.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(target);
            }
            return BitConverter.ToInt32 (target, 0);
        }

        public static long ExtractLong(this byte[] byteValue, int offset)
        {
            byte[] target = new byte[8];
            Array.Copy(byteValue, offset, target, 0, target.Length);
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(target);
            }
            return BitConverter.ToInt64(target, 0);
        }

        public static byte[] ConvertToByteArray(this long longValue)
        {
            byte[] byteArray = LongToByteArray (longValue);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(byteArray);
            }
            return byteArray;
        }

        private  static byte[] LongToByteArray( long value)
        {
            return new byte[]{
                (byte) (value >> 56),
                (byte) (value >> 48),
                (byte) (value >> 40),
                (byte) (value >> 32),
                (byte) (value >> 24),
                (byte) (value >> 16),
                (byte) (value >> 8),
                (byte) value
                };
        }

        public static byte[] ConvertToByteArray(this ushort shortValue)
        {
            byte[] byteArray = BitConverter.GetBytes(shortValue);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(byteArray);
            }
            return byteArray;
        }
    }
}
