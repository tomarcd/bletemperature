﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BluetoothLE;

namespace BLETemperature.Core.BLEExtensions
{
   
    public static class SHT3xHumidityExtension
    {
        public static Guid SHT3xHumidityServiceUuid = new Guid("00001234-b38d-4985-720e-0f993a68ee41");

        private static String SHT3XHUMIDITY_LEVEL_CHARACTERISTIC_UUID = "00001235-b38d-4985-720e-0f993a68ee41";

        public static String SHT3XHUMIDITY_LEVEL_UNIT = "%";


        public static IObservable<IScanResult> ScanForSHT3xHumidityService(this IAdapter adapter)
        {
            return adapter
                .Scan()
                .Where(x => x.AdvertisementData.ServiceUuids?.Contains(SHT3xHumidityServiceUuid) ?? false);
        }


        public static async Task<bool> HasSHT3xHumidityService(this IDevice device)
        {
            AssertHumidityServiceConnected(device);
            var character = await FindHumidityServiceCharacteristic(device);
            return character != null;
        }


        public static IObservable<float> WhenSHT3xHumidityLevelChange(this IDevice device)
        {
            AssertHumidityServiceConnected(device);

            return Observable.Create<float>(async ob =>
            {
                IDisposable token = null;
                var ch = await FindHumidityServiceCharacteristic(device);

                if (ch == null)
                {
                    ob.OnError(new ArgumentException("Device does not appear to be a SHT3xHumidity"));
                }
                else
                {

                    token = ch
                        //.ReadInterval(TimeSpan.FromSeconds(3))
                        .WhenReadOrNotify(TimeSpan.FromSeconds(3))
                        .Subscribe(result =>
                        {
                            if (result.Data.Length < 4 * 2 || result.Data.Length % 4 > 0)
                            {
                                var humidityLevel = result.Data.ExtractFloat(0);
                                ob.OnNext(humidityLevel);
                            }
                        });
                }
                return () => token?.Dispose();
            });
        }


        static void AssertHumidityServiceConnected(IDevice device)
        {
            if (device.Status != ConnectionStatus.Connected)
                throw new ArgumentException("Device must be connected");
        }


        public static async Task<IGattCharacteristic> FindHumidityServiceCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(SHT3xHumidityServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                //.Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(SHT3XHUMIDITY_LEVEL_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }
    }
}
