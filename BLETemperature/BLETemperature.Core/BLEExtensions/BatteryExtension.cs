﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Plugin.BluetoothLE;

namespace BLETemperature.Core.BLEExtensions
{
    

    public static class BatteryExtension
    {
        public static Guid BatteryServiceUuid = new Guid("0000180f-0000-1000-8000-00805f9b34fb");

        private static String BATTERY_LEVEL_CHARACTERISTIC_UUID = "00002a19-0000-1000-8000-00805f9b34fb"; 

        public static  String BATTERY_LEVEL_UNIT = "%";


        public static IObservable<IScanResult> ScanForBatteryService(this IAdapter adapter)
        {
            return adapter
                .Scan()
                .Where(x => x.AdvertisementData.ServiceUuids?.Contains(BatteryServiceUuid) ?? false);
        }


        public static async Task<bool> HasBatteryService(this IDevice device)
        {
            AssertConnected(device);
            var character = await FindCharacteristic(device);
            return character != null;
        }


        public static IObservable<ushort> WhenBatteryLevelChange(this IDevice device)
        {
            AssertConnected(device);

            return Observable.Create<ushort>(async ob =>
            {
                IDisposable token = null;
                IDisposable token2 = null;
                var ch = await FindCharacteristic(device);

                if (ch == null)
                {
                    ob.OnError(new ArgumentException("Device does not appear to be a Battery Service"));
                }
                else
                {
                    
                    token = ch
                        //.ReadInterval(TimeSpan.FromSeconds(3))
                        .WhenReadOrNotify(TimeSpan.FromSeconds(3))
                        .Subscribe(result =>
                        {

                            var batteryLevel = (ushort)result.Data[0];
                            ob.OnNext(batteryLevel);
                        });
                    token2 = ch.Read()
                        .Subscribe(result =>
                        {

                            var batteryLevel = (ushort)result.Data[0];
                            ob.OnNext(batteryLevel);
                        });
                }
                return () => { 
                    token?.Dispose();
                    token2?.Dispose();
                };
            });
        }


        static void AssertConnected(IDevice device)
        {
            if (device.Status != ConnectionStatus.Connected)
                throw new ArgumentException("Device must be connected");
        }


        static async Task<IGattCharacteristic> FindCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(BatteryServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                //.Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(BATTERY_LEVEL_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }
    }
}
