﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross.Platform;
using Plugin.BluetoothLE;

namespace BLETemperature.Core.BLEExtensions
{


    public static class SHT3xHistoryExtension
    {
        public static Guid SHT3xHistoryServiceUuid = new Guid("0000f234-b38d-4985-720e-0f993a68ee41");


        private static Guid SYNC_TIME_CHARACTERISTIC_UUID = new Guid("0000f235-b38d-4985-720e-0f993a68ee41");
        private static Guid READ_BACK_TO_TIME_MS_CHARACTERISTIC_UUID = new Guid("0000f236-b38d-4985-720e-0f993a68ee41");
        private static Guid NEWEST_SAMPLE_TIME_MS_CHARACTERISTIC_UUID = new Guid("0000f237-b38d-4985-720e-0f993a68ee41");
        private static Guid START_LOGGER_DOWNLOAD_CHARACTERISTIC_UUID = new Guid("0000f238-b38d-4985-720e-0f993a68ee41");
        private static Guid LOGGER_INTERVAL_MS_CHARACTERISTIC_UUID = new Guid("0000f239-b38d-4985-720e-0f993a68ee41");

        private static byte DATA_POINT_SIZE = 4;

        private static int TIMEOUT_MS = 2000;

        public static int SHT3xHistoryLoggerIntervalMs = 0;

        public static IObservable<IScanResult> ScanForSHT3xHistoryService(this IAdapter adapter)
        {
            return adapter
                .Scan()
                .Where(x => x.AdvertisementData.ServiceUuids?.Contains(SHT3xHistoryServiceUuid) ?? false);
        }


        public static async Task<bool> HasSHT3xHistoryService(this IDevice device)
        {
            AssertSHT3xHistoryConnected(device);
            var character = await FindSHT3xHistoryStartLoggerDownloadCharacteristic(device);
            return character != null;
        }


        public static IObservable<Tuple<int, List<SHT3xHistoryDataObject>>> WhenSHT3xHistoryChange(this IDevice device)
        {
            AssertSHT3xHistoryConnected(device);

            return Observable.Create<Tuple<int, List<SHT3xHistoryDataObject>>>(async ob =>
            {
                IDisposable token = null;
                IDisposable token2 = null;

                var syncTimeCharacteristic = await FindSHT3xHistorySyncTimeCharacteristic(device);
                var readBackToTimeMSCharacteristic = await FindSHT3xHistoryReadBackToTimeMSCharacteristic(device);
                var newestSampleTimeMSCharacteristic = await FindSHT3xHistoryNewestSampleTimeMSCharacteristic(device);
                var startLoggerDownloadCharacteristic = await FindSHT3xHistoryStartLoggerDownloadCharacteristic(device);
                var loggerIntervalMSCharacteristic = await FindSHT3xHistoryLoggerIntervalMSCharacteristic(device);
                var temperatureServiceCharacteristic = await SHT3xTemperatureExtension.FindTemperatureServiceCharacteristic(device);
                var humidityServiceCharacteristic = await SHT3xHumidityExtension.FindHumidityServiceCharacteristic(device);

                //var characteristics = await FindSHT3xHistoryCharacteristics(device);
                //var syncTimeCharacteristic = characteristics.Where((characteristic) => characteristic.Uuid.Equals(SYNC_TIME_CHARACTERISTIC_UUID)).FirstOrDefault();
                //var readBackToTimeMSCharacteristic = characteristics.Where((characteristic) => characteristic.Uuid.Equals(READ_BACK_TO_TIME_MS_CHARACTERISTIC_UUID)).FirstOrDefault();
                //var newestSampleTimeMSCharacteristic = characteristics.Where((characteristic) => characteristic.Uuid.Equals(NEWEST_SAMPLE_TIME_MS_CHARACTERISTIC_UUID)).FirstOrDefault();
                //var startLoggerDownloadCharacteristic = characteristics.Where((characteristic) => characteristic.Uuid.Equals(START_LOGGER_DOWNLOAD_CHARACTERISTIC_UUID)).FirstOrDefault();
                //var loggerIntervalMSCharacteristic = characteristics.Where((characteristic) => characteristic.Uuid.Equals(LOGGER_INTERVAL_MS_CHARACTERISTIC_UUID)).FirstOrDefault();
                //var temperatureServiceCharacteristic = await SHT3xTemperatureExtension.FindTemperatureServiceCharacteristic(device);
                //var humidityServiceCharacteristic = await SHT3xHumidityExtension.FindHumidityServiceCharacteristic(device);

                var mNewestSampleTimeMs = 0L;
                var mOldestSampleTimeMs = 0L;
                var mNrOfElementsDownloaded = 0;
                var mNrOfElementsToDownload = 0;
                var downloadedDataObject = new Dictionary<int, SHT3xHistoryDataObject>();

                if (syncTimeCharacteristic == null
                    || readBackToTimeMSCharacteristic == null
                    || newestSampleTimeMSCharacteristic == null
                    || startLoggerDownloadCharacteristic == null
                    || loggerIntervalMSCharacteristic == null
                    || temperatureServiceCharacteristic == null
                    || humidityServiceCharacteristic == null
                   )
                {
                    ob.OnError(new ArgumentException("Device does not appear to be a SHT3xHistory"));
                }
                else
                {
                    var syncTimeCharacteristicResult = await syncTimeCharacteristic.Write(CurrentTimeMillis().ConvertToByteArray());
                    var loggerIntervalMSCharacteristicResult = await loggerIntervalMSCharacteristic.Read();
                    SHT3xHistoryLoggerIntervalMs = loggerIntervalMSCharacteristicResult.Data.ExtractInt(0);
                    var newestSampleTimeMSCharacteristicResult = await newestSampleTimeMSCharacteristic.Read();
                    mNewestSampleTimeMs = newestSampleTimeMSCharacteristicResult.Data.ExtractLong(0);
                    var readBackToTimeMSCharacteristicResult = await readBackToTimeMSCharacteristic.Read();
                    mOldestSampleTimeMs = readBackToTimeMSCharacteristicResult.Data.ExtractLong(0);
                    mNrOfElementsToDownload = (int)Math.Floor((double)((mNewestSampleTimeMs - mOldestSampleTimeMs) /SHT3xHistoryLoggerIntervalMs));
                    if (mNrOfElementsToDownload == 0)
                    {
                        ob.OnError(new NoDataException("SHT3xHistory Service No Data found"));
                    }
                    else
                    {
                        byte[] startloggerValue = new byte[] { 1 };
                        startLoggerDownloadCharacteristic.WriteWithoutResponse(startloggerValue);
                        var notificationEnabled = await startLoggerDownloadCharacteristic.EnableNotifications();
                        token = temperatureServiceCharacteristic.WhenReadOrNotify(TimeSpan.FromSeconds(3))
                                                                .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                                                .ObserveOn(NewThreadScheduler.Default)
                                      .Subscribe(result =>
                                      {
                                          if (result.Data.Length < 4 * 2 || result.Data.Length % 4 > 0)
                                          {
                                              //ob.OnError(new Exception("parseHistoryValue -> Received History value does not have a valid length."));
                                          }
                                          else
                                          {
                                              var returnValue = ProcessData(result.Data, downloadedDataObject, mNewestSampleTimeMs, SHT3xHistoryLoggerIntervalMs, mNrOfElementsToDownload, ref mNrOfElementsDownloaded, true);
                                              if (mNrOfElementsDownloaded >= mNrOfElementsToDownload)
                                              {
                                                  ob.Respond(returnValue);
                                              }
                                              else
                                              {
                                                  ob.OnNext(returnValue);
                                              }
                                          }
                                      });
                        token2 = humidityServiceCharacteristic.WhenReadOrNotify(TimeSpan.FromSeconds(3))
                                                              .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                                                .ObserveOn(NewThreadScheduler.Default)
                                      .Subscribe(result =>
                                      {
                                          if (result.Data.Length < 4 * 2 || result.Data.Length % 4 > 0)
                                          {
                                              //ob.OnError(new Exception("parseHistoryValue -> Received History value does not have a valid length."));
                                          }
                                          else
                                          {
                                              var returnValue = ProcessData(result.Data, downloadedDataObject, mNewestSampleTimeMs, SHT3xHistoryLoggerIntervalMs, mNrOfElementsToDownload, ref mNrOfElementsDownloaded, false);
                                              if (mNrOfElementsDownloaded >= mNrOfElementsToDownload)
                                              {
                                                  ob.Respond(returnValue);
                                              }
                                              else
                                              {
                                                  ob.OnNext(returnValue);
                                              }
                                          }
                                      });
                    }

                }
                return () =>
                {
                    token?.Dispose();
                    token2?.Dispose();
                };
            });
        }


        static void AssertSHT3xHistoryConnected(IDevice device)
        {
            if (device.Status != ConnectionStatus.Connected)
                throw new ArgumentException("Device must be connected");
        }


        static async Task<IGattCharacteristic> FindSHT3xHistorySyncTimeCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(SHT3xHistoryServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                .Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(SYNC_TIME_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }

        static async Task<IGattCharacteristic> FindSHT3xHistoryReadBackToTimeMSCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(SHT3xHistoryServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                .Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(READ_BACK_TO_TIME_MS_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }

        static async Task<IGattCharacteristic> FindSHT3xHistoryNewestSampleTimeMSCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(SHT3xHistoryServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                .Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(NEWEST_SAMPLE_TIME_MS_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }

        static async Task<IGattCharacteristic> FindSHT3xHistoryStartLoggerDownloadCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                .Where(x => x.Uuid.Equals(SHT3xHistoryServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                .Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(START_LOGGER_DOWNLOAD_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }

        static async Task<IGattCharacteristic> FindSHT3xHistoryLoggerIntervalMSCharacteristic(IDevice device)
        {
            return await device
                .WhenServiceDiscovered()
                .Where(x => x.Uuid.Equals(SHT3xHistoryServiceUuid))
                .SelectMany(x => x.WhenCharacteristicDiscovered())
                .Where((IGattCharacteristic characteristic) => characteristic.Uuid.Equals(LOGGER_INTERVAL_MS_CHARACTERISTIC_UUID))
                .FirstOrDefaultAsync();
        }


        //static async Task<List<IGattCharacteristic>> FindSHT3xHistoryCharacteristics(IDevice device)
        //{
        //    var returnlist = new List<IGattCharacteristic>();
        //    await device
        //        .WhenServiceDiscovered()
        //        .Where(x => x.Uuid.Equals(SHT3xHistoryServiceUuid))
        //        .SelectMany(x => x.WhenCharacteristicDiscovered())
        //        .ForEachAsync((characteristic)=>{
        //            returnlist.Add(characteristic);
        //        });

        //        return returnlist;
        //}

        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
        }

        public static DateTime FromUnixTime(long unixTimeMillis)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTimeMillis);
        }

        private static Tuple<int, List<SHT3xHistoryDataObject>> ProcessData(byte[] data, Dictionary<int, SHT3xHistoryDataObject> downloadedDataObject, long newestSampleTimeMs, long loggerIntervalMs, int numberOfElementsToDownload, ref int numberOfElementsDownloaded, bool isTemperatureDataSet)
        {
            lock (downloadedDataObject)
            {
                var target = new byte[4];
                Array.Copy(data, target, target.Length);
                var baseSequenceNr = target.ExtractInt(0);

                var size = data.Length / DATA_POINT_SIZE;
                int valueSequenceNr = 0;
                float currentValue = 0;
                SHT3xHistoryDataObject currentSHT3xHistoryDataObject = null;
                List<SHT3xHistoryDataObject> completedDownloadDataList = new List<SHT3xHistoryDataObject>();

                for (int i = 1; i < size; i++)
                {
                    valueSequenceNr = baseSequenceNr + i - 1;
                    currentValue = data.ExtractFloat(i * DATA_POINT_SIZE);

                    if (downloadedDataObject.ContainsKey(valueSequenceNr))
                    {
                        currentSHT3xHistoryDataObject = downloadedDataObject[valueSequenceNr];
                        if (isTemperatureDataSet)
                        {
                            currentSHT3xHistoryDataObject.Temperature = currentValue;
                        }
                        else
                        {
                            currentSHT3xHistoryDataObject.Humidity = currentValue;
                        }

                        if (currentSHT3xHistoryDataObject.Temperature.HasValue && currentSHT3xHistoryDataObject.Humidity.HasValue)
                        {
                            numberOfElementsDownloaded += 1;
                            completedDownloadDataList.Add(currentSHT3xHistoryDataObject);

                            downloadedDataObject.Remove(valueSequenceNr);
                        }
                    }
                    else
                    {
                        var theTimeStamp = newestSampleTimeMs - (valueSequenceNr * loggerIntervalMs);
                        downloadedDataObject.Add(valueSequenceNr, new SHT3xHistoryDataObject()
                        {
                            TimeStamp = theTimeStamp,
                            Time = FromUnixTime(theTimeStamp),
                            SequenceNumber = valueSequenceNr,
                            Temperature = isTemperatureDataSet ? currentValue : (float?)null,
                            Humidity = isTemperatureDataSet ? (float?)null : currentValue
                        });
                    }
                }


                Mvx.Warning("------------numberOfElementsDownloaded={0}",numberOfElementsDownloaded);
                return new Tuple<int, List<SHT3xHistoryDataObject>>(Math.Min((int) Math.Ceiling(100 * (numberOfElementsDownloaded / (float)numberOfElementsToDownload)),100), completedDownloadDataList);
            }
        }

        public class SHT3xHistoryDataObject
        {
            public long TimeStamp { get; set; }
            public DateTime Time { get; set; }
            public int SequenceNumber { get; set; }
            public float? Temperature { get; set; }
            public float? Humidity { get; set; }
        }

        public class NoDataException : Exception
        {
            public NoDataException()
            {
            }

            public NoDataException(string message) : base(message)
            {
            }

            public NoDataException(string message, Exception innerException) : base(message, innerException)
            {
            }

            protected NoDataException(SerializationInfo info, StreamingContext context) : base(info, context)
            {
            }
        }
    }
}
