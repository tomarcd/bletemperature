﻿using System;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using Plugin.BluetoothLE;
using Plugin.Permissions.Abstractions;
using BLETemperature.Core.BLEExtensions;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Collections.ObjectModel;
using static BLETemperature.Core.BLEExtensions.SHT3xHistoryExtension;
using System.Collections.Generic;

namespace BLETemperature.Core.ViewModels
{
    public class ConnectToDeviceViewModel : MvxViewModel<ListModel>
    {
        readonly IMvxNavigationService _NavigationService;
        readonly IUserDialogs _UserDialogs;
        readonly IPermissions _Permissions;
        readonly IAdapter _BleAdapter;

        ListModel _Device;

        IDisposable _BatteryObserverLevel;
        IDisposable _HumidityObserverLevel;
        IDisposable _TemperatureObserverLevel;
        IDisposable _SHT3xHistoryObserver;
        IDisposable _StatusObserver;
        IDisposable _connectionObserver;

        public ConnectToDeviceViewModel(IMvxNavigationService navigationService, IUserDialogs userDialogs, IPermissions permissions, IAdapter bleAdapter)
        {
            _NavigationService = navigationService;
            _UserDialogs = userDialogs;
            _Permissions = permissions;
            _BleAdapter = bleAdapter;
            HistoryDataList = new MvxObservableCollection<SHT3xHistoryDataObject>();
        }

        public override void Prepare(ListModel parameter)
        {
            _Device = parameter;
            DeviceName = _Device.Name;
            DeviceUuid = _Device.UuidString;
            //UpdateStatus(_Device.Device.Status);

            _StatusObserver = _Device.Device
                                     .WhenStatusChanged()
                                     //.SubscribeOn(NewThreadScheduler.Default)
                                     .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                     .Subscribe(UpdateStatus);
        }


        public override void ViewDestroy(bool viewFinishing = true)
        {
            base.ViewDestroy(viewFinishing);
            _BatteryObserverLevel?.Dispose();
            _TemperatureObserverLevel?.Dispose();
            _HumidityObserverLevel?.Dispose();
            _StatusObserver?.Dispose();
            _SHT3xHistoryObserver?.Dispose();
        }

        void UpdateStatus(ConnectionStatus x)
        {
            this.Status = x;

            switch (x)
            {
                case ConnectionStatus.Disconnecting:
                case ConnectionStatus.Connecting:
                    this.ActionText = x.ToString();
                    break;

                case ConnectionStatus.Disconnected:
                    this.ActionText = "Connect";
                    DisposeDeviceService();
                    break;
                case ConnectionStatus.Connected:
                    this.ActionText = "Disconnect";
                    Task.Run(ReadBatteryLevel);
                    Task.Run(ReadHumidityLevel);
                    Task.Run(ReadTemperatureLevel);
                    break;
            }
        }

        void DisposeDeviceService()
        {
            _BatteryObserverLevel?.Dispose();
            _TemperatureObserverLevel?.Dispose();
            _HumidityObserverLevel?.Dispose();
            _SHT3xHistoryObserver?.Dispose();
        }

        async Task ReadAllServices()
        {
            await ReadBatteryLevel();
            await ReadHumidityLevel();
            await ReadTemperatureLevel();
        }

        async Task ReadSHT3xHistoryServices()
        {
            try
            {
                _SHT3xHistoryObserver?.Dispose();
                bool hasSHT3xHistoryService = await _Device.Device.HasSHT3xHistoryService();
                if (hasSHT3xHistoryService)
                {
                    using (var cancelSrc = new CancellationTokenSource())
                    {
                        using (var progress = _UserDialogs.Progress("Downloading History", cancelSrc.Cancel, "Cancel"))
                        {
                            HistoryDataList.Clear();
                            System.Timers.Timer aTimer = new System.Timers.Timer();
                            aTimer.Elapsed += (sender, e) => {
                                cancelSrc.Cancel();
                            };
                            aTimer.Interval = 5000;
                            aTimer.Start();
                            await _Device.Device.WhenSHT3xHistoryChange()
                                         .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                         .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                         //.ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                         //.SubscribeOn(NewThreadScheduler.Default)
                                         .ForEachAsync((historyDataTuple) =>
                            {
                                aTimer.Stop();
                                progress.PercentComplete = historyDataTuple.Item1;
                                HistoryDataList.AddRange(historyDataTuple.Item2);

                                aTimer = new System.Timers.Timer();
                                aTimer.Elapsed += (sender, e) => {
                                    cancelSrc.Cancel();
                                };
                                aTimer.Interval = 10000;
                                aTimer.Start();

                                //HistoryDataList.Sort((SHT3xHistoryDataObject a, SHT3xHistoryDataObject b) => a.TimeStamp.CompareTo(b) * -1);
                            }, cancelSrc.Token).ConfigureAwait(true);
                            int asdsad = 0;
                        }
                    }
                }
                else
                {
                    //BatteryLevel = "BatteryLevel service not found";
                }
            }catch (Exception ex)
            {
                //_UserDialogs.Alert(ex.ToString());
            }
        }

        async Task ReadBatteryLevel()
        {
            _BatteryObserverLevel?.Dispose();
            bool hasBatteryService = await _Device.Device.HasBatteryService();
            if (hasBatteryService)
            {
                _BatteryObserverLevel = _Device.Device.WhenBatteryLevelChange().Subscribe((batteryLevel) =>
                {
                    BatteryLevel = string.Format("BatteryLevel {0} %", batteryLevel);
                });
            }
            else
            {
                BatteryLevel = "BatteryLevel service not found";
            }
        }

        async Task ReadHumidityLevel()
        {
            _HumidityObserverLevel?.Dispose();
            bool hasSHT3xHumidityService = await _Device.Device.HasSHT3xHumidityService();
            if (hasSHT3xHumidityService)
            {
                _HumidityObserverLevel = _Device.Device.WhenSHT3xHumidityLevelChange().Subscribe((humidityLevel) =>
                {
                    HumidityLevel = string.Format("HumidityLevel {0:N2} %", humidityLevel);
                });
            }
            else
            {
                HumidityLevel = "HumidityLevel service not found";
            }
        }

        async Task ReadTemperatureLevel()
        {
            _TemperatureObserverLevel?.Dispose();
            bool hasSHT3xTemperatureService = await _Device.Device.HasSHT3xTemperatureService();
            if (hasSHT3xTemperatureService)
            {
                _TemperatureObserverLevel = _Device.Device.WhenSHT3xTemperatureLevelChange().Subscribe((temperatureLevel) =>
                {
                    TemperatureLevel = string.Format("TemperatureLevel {0:N2} °C", temperatureLevel);
                });
            }
            else
            {
                HumidityLevel = "TemperatureLevel service not found";
            }
        }

        public MvxObservableCollection<SHT3xHistoryDataObject> HistoryDataList { get; set; }

        string _DeviceName = "Connect";
        public string DeviceName
        {
            get { return _DeviceName; }
            set
            {
                SetProperty(ref _DeviceName, value);
            }
        }

        string _DeviceUuid = "Connect";
        public string DeviceUuid
        {
            get { return _DeviceUuid; }
            set
            {
                SetProperty(ref _DeviceUuid, value);
            }
        }

        string _DownloadActionText = "Download";

        public string DownloadActionText
        {
            get { return _DownloadActionText; }
            set
            {
                SetProperty(ref _DownloadActionText, value);
            }
        }

        string _ActionText = "Connect";

        public string ActionText
        {
            get { return _ActionText; }
            set
            {
                SetProperty(ref _ActionText, value);
            }
        }

        string _BatteryLevel = "BatteryLevel NA";

        public string BatteryLevel
        {
            get { return _BatteryLevel; }
            set
            {
                SetProperty(ref _BatteryLevel, value);
            }
        }

        string _TemperatureLevel = "TemperatureLevel NA";

        public string TemperatureLevel
        {
            get { return _TemperatureLevel; }
            set
            {
                SetProperty(ref _TemperatureLevel, value);
            }
        }

        string _HumidityLevel = "HumidityLevel NA";

        public string HumidityLevel
        {
            get { return _HumidityLevel; }
            set
            {
                SetProperty(ref _HumidityLevel, value);
            }
        }


        public bool IsConnected
        {
            get { return Status == ConnectionStatus.Connected; }
        }

        public bool IsConnecting
        {
            get { return Status == ConnectionStatus.Connecting; }
        }
        public bool IsDisconnected
        {
            get { return Status == ConnectionStatus.Disconnected; }
        }
        public bool IsDisconnecting
        {
            get { return Status == ConnectionStatus.Disconnecting; }
        }

        ConnectionStatus _Status = ConnectionStatus.Disconnected;
        public ConnectionStatus Status
        {
            get { return _Status; }
            set
            {
                SetProperty(ref _Status, value);

                RaisePropertyChanged(() => IsConnected);
                RaisePropertyChanged(() => IsConnecting);
                RaisePropertyChanged(() => IsDisconnected);
                RaisePropertyChanged(() => IsDisconnecting);
            }
        }


        public ICommand StartScanningCommand => new MvxAsyncCommand(DoConnectCommand);
        private async Task DoConnectCommand()
        {
            try
            {
                // don't cleanup connection - force user to d/c
                if (_Device.Device.Status == ConnectionStatus.Disconnected)
                {
                    using (var cancelSrc = new CancellationTokenSource())
                    {
                        using (_UserDialogs.Loading("Connecting", cancelSrc.Cancel, "Cancel"))
                        {

                            await _Device.Device.Connect()
                                         //.Take(1)
                                         .ToTask(cancelSrc.Token);
                            int asdsad = 0;
                        }
                    }
                }
                else
                {
                    _Device.Device.CancelConnection();
                }
            }
            catch (Exception ex)
            {
                _UserDialogs.Alert(ex.ToString());
            }
        }

        public ICommand StartDownloadCommand => new MvxCommand(()=>Task.Run(ReadSHT3xHistoryServices));

        public ICommand PairToDeviceCommand => new MvxCommand(DoPairToDeviceCommand);

        private void DoPairToDeviceCommand()
        {
            if (!_Device.Device.Features.HasFlag(DeviceFeatures.PairingRequests))
            {
                _UserDialogs.Alert("Pairing is not supported on this platform");
            }
            else if (_Device.Device.PairingStatus == PairingStatus.Paired)
            {
                _UserDialogs.Alert("Device is already paired");
            }
            else
            {
                _Device.Device.PairingRequest();
            }
        }

    }
}
