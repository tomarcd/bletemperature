﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using BLETemperature.Core.BLEExtensions;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using Plugin.BluetoothLE;
using Plugin.Permissions.Abstractions;

namespace BLETemperature.Core.ViewModels
{
    public class ListModel : MvxViewModel
    {
        IDevice _Device;
        public IDevice Device
        {
            get { return _Device; }
            set
            {
                SetProperty(ref _Device, value);
            }
        }

        bool _IsConnected;
        public bool IsConnected
        {
            get { return _IsConnected; }
            set
            {
                SetProperty(ref _IsConnected, value);
            }
        }

        Guid _Uuid;
        public Guid Uuid
        {
            get { return _Uuid; }
            set
            {
                SetProperty(ref _Uuid, value);
                _UuidString = Uuid == null ? null : _Uuid.ToString();
            }
        }

        String _UuidString;
        public String UuidString
        {
            get { return _UuidString; }
            set
            {
                SetProperty(ref _UuidString, value);
            }
        }


        string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                SetProperty(ref _Name, value);
            }
        }

        int _Rssi;
        public int Rssi
        {
            get { return _Rssi; }
            set
            {
                SetProperty(ref _Rssi, value);
            }
        }

        bool _IsConnectable;
        public bool IsConnectable
        {
            get { return _IsConnectable; }
            set
            {
                SetProperty(ref _IsConnectable, value);
            }
        }

        int _ServiceCount;
        public int ServiceCount
        {
            get { return _ServiceCount; }
            set
            {
                SetProperty(ref _ServiceCount, value);
            }
        }

        string _ManufacturerData;
        public string ManufacturerData
        {
            get { return _ManufacturerData; }
            set
            {
                SetProperty(ref _ManufacturerData, value);
            }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set
            {
                SetProperty(ref _LocalName, value);
            }
        }

        int _TxPower;
        public int TxPower
        {
            get { return _TxPower; }
            set
            {
                SetProperty(ref _TxPower, value);
            }
        }

        public bool TrySet(IScanResult result)
        {
            var response = false;

            if (this.Uuid == Guid.Empty)
            {
                this.Device = result.Device;
                this.Uuid = this.Device.Uuid;

                response = true;
            }

            try
            {
                if (this.Uuid == result.Device.Uuid)
                {
                    response = true;

                    this.Name = result.Device.Name;
                    this.Rssi = result.Rssi;

                    var ad = result.AdvertisementData;
                    this.ServiceCount = ad.ServiceUuids?.Length ?? 0;
                    this.IsConnectable = ad.IsConnectable;
                    this.LocalName = ad.LocalName;
                    this.TxPower = ad.TxPower;
                    this.ManufacturerData = ad.ManufacturerData == null
                        ? null
                        : BitConverter.ToString(ad.ManufacturerData);
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.ToString());
            }
            return response;
        }
    }

    public class ScanForDevicesViewModel : MvxViewModel, IDisposable
    {
        readonly IMvxNavigationService _NavigationService;
        readonly IUserDialogs _UserDialogs;
        readonly IPermissions _Permissions;
        readonly IAdapter _BleAdapter;


        IDisposable scan;
        IDisposable scanStatus;
        IDisposable connect;
        IDisposable poweredOnStatus;

        public ScanForDevicesViewModel(IMvxNavigationService navigationService, IUserDialogs userDialogs, IPermissions permissions, IAdapter bleAdapter)
        {
            _NavigationService = navigationService;
            _UserDialogs = userDialogs;
            _Permissions = permissions;
            _BleAdapter = bleAdapter;
            //TODO move
            SynchronizationContextHelper.UIThreadSynchronizationContext = SynchronizationContext.Current;
            DevicesFound = new ObservableCollection<ListModel>();
            this.connect = this._BleAdapter
                                .WhenDeviceStatusChanged()
                                .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                                .Subscribe(x =>
                                {
                                    var vm = this.DevicesFound.FirstOrDefault(listModel => listModel.Device.Uuid.Equals(x.Uuid));
                                    if (vm != null)
                                        vm.IsConnected = x.Status == ConnectionStatus.Connected;
                                });

            scanStatus = this._BleAdapter
                             .WhenScanningStatusChanged()
                             .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                             .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                             .Subscribe(ScanStatusChange);

            poweredOnStatus = this._BleAdapter
                .WhenStatusChanged()
                .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                .Subscribe(x =>
                {
                    this.IsSupported = x == AdapterStatus.PoweredOn;

                });
        }

        private void ScanStatusChange(bool on)
        {
            this.IsScanning = on;
            this.ActionText = on ? "Stop Scan" : "Scan";
        }


        public override void ViewAppeared()
        {
            base.ViewAppeared();


        }

        private void CleanUp()
        {
            this.scan?.Dispose();
            this.connect?.Dispose();
            this.scanStatus?.Dispose();
            this.poweredOnStatus?.Dispose();
            this.ScanStatusChange(false);
        }
        public override void ViewDisappeared()
        {
            base.ViewDisappeared();
            this.scan?.Dispose();
            this.ScanStatusChange(false);
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            base.ViewDestroy(viewFinishing);
            CleanUp();
        }

        bool _IsScanning = false;

        public bool IsScanning
        {
            get { return _IsScanning; }
            set
            {
                SetProperty(ref _IsScanning, value);
            }
        }

        bool _IsSupported;
        public bool IsSupported
        {
            get { return _IsSupported; }
            set
            {
                
                _IsSupported = value;
                RaisePropertyChanged(() => IsSupported);
                StartScanningCommand.RaiseCanExecuteChanged();
                StartScanningCommand.CanExecute();
            }
        }



        string _ActionText = "Scan";

        public string ActionText
        {
            get { return _ActionText; }
            set
            {
                SetProperty(ref _ActionText, value);
            }
        }


        ListModel _SelectedListModel = null;

        public ListModel SelectedListModel
        {
            get { return _SelectedListModel; }
            set
            {

                SetProperty(ref _SelectedListModel, value);
            }
        }


        public ObservableCollection<ListModel> DevicesFound { get; set; }

        public MvxCommand StartScanningCommand => new MvxCommand(() =>
        {
            if (this.IsSupported)
            {
                if (this.IsScanning)
                {
                    this.scan?.Dispose();
                    ScanStatusChange(false);
                }
                else
                {
                    this.DevicesFound.Clear();
                    ScanStatusChange(true);

                    var config = new ScanConfig();
                    //config.ServiceUuids.Add(SHT3xTemperatureExtension.SHT3xTemperatureServiceUuid);
                    //config.ServiceUuids.Add(SHT3xHumidityExtension.SHT3xHumidityServiceUuid);
                    //config.ServiceUuids.Add(SHT3xHistoryExtension.SHT3xHistoryServiceUuid);

                    this.scan = this._BleAdapter
                        .Scan(config)
                        .ObserveOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                        .SubscribeOn(SynchronizationContextHelper.UIThreadSynchronizationContext)
                        .Subscribe(this.OnScanResult);
                }
            }
            else
            {
                if (_BleAdapter.CanOpenSettings())
                {
                    CrossBleAdapter.Current.OpenSettings();
                }
                else
                {
                    _UserDialogs.Alert("Please turn on bluetooth", "Bluetooth Is OFF");
                }
            }

        });

        private readonly string _LocalName = "Smart Humigadget";
        void OnScanResult(IScanResult result)
        {
            if (result.AdvertisementData.LocalName == _LocalName
                || result.AdvertisementData.ServiceUuids.
                Where((guid)=> guid.Equals(SHT3xTemperatureExtension.SHT3xTemperatureServiceUuid)
                      || guid.Equals(SHT3xHumidityExtension.SHT3xHumidityServiceUuid)
                      || guid.Equals(SHT3xHistoryExtension.SHT3xHistoryServiceUuid)
                     ).Any()
               )
            {
                var dev = this.DevicesFound.FirstOrDefault(listModel => listModel.Device.Uuid.Equals(result.Device.Uuid));
                if (dev != null)
                {
                    dev.TrySet(result);
                }
                else
                {
                    dev = new ListModel();
                    dev.TrySet(result);
                    this.DevicesFound.Add(dev);
                }
            }

        }

        private async Task DoStartScanningCommand()
        {
            var status = await _Permissions.CheckPermissionStatusAsync(Permission.Location);
        }


        public ICommand SelectDeviceCommand => new MvxAsyncCommand<ListModel>(DOSelectDeviceCommand);

        async Task DOSelectDeviceCommand(ListModel item)
        {
            if (item != null)
            {
                SelectedListModel = null;
                await _NavigationService.Navigate<ConnectToDeviceViewModel, ListModel>(item);
            }
        }

        public void Dispose()
        {
            this.CleanUp();
        }
    }
}
