﻿using System;
using System.Threading;

namespace BLETemperature.Core
{
    public static class SynchronizationContextHelper
    {
        public static SynchronizationContext UIThreadSynchronizationContext { get; set;}
    }
}
