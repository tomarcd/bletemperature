﻿using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.Droid;
using MvvmCross.Platform.Platform;
using BLETemperature.Core;
using MvvmCross.Forms.Platform;
using MvvmCross.Forms.Droid.Platform;

namespace BLETemperature.Droid
{
    public class Setup : MvxFormsAndroidSetup
    {
        public Setup(Context applicationContext)
            : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp() => new CoreApp();
        protected override MvxFormsApplication CreateFormsApplication() => new Application();
        protected override IMvxTrace CreateDebugTrace() => new DebugTrace();
    }
}
